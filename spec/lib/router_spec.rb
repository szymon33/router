require 'spec_helper'
require 'router'

describe Router do
  before(:each) { allow(described_class).to receive(:print) }
  it { respond_to :routing_table }
  it { respond_to :REQUEST_METHODS }

  let(:r) { described_class.new }

  describe '#initialize' do
    it 'has routing_table array' do
      expect(r.routing_table).to be_kind_of(Array)
      expect(r.routing_table).to be_empty
    end
  end

  describe '#draw' do
    it 'without block' do
      r.draw do
        match '/' => 'downloads#index'
        match '/movie' => 'downloads#show'
      end
      expect(r).to receive(:print).exactly(2).times
      r.draw
    end

    it 'is valid with many arguments' do
      path = r.draw do
        match '/downloads/:id/movie' => 'downloads#movie',
              via: :get, as: 'movie'
      end.first

      expect(path.helper).to eql 'movie'
      expect(path.method).to eql :get
      expect(path.pattern).to eql '/downloads/:id/movie'
      expect(path.exec).to eql 'downloads#movie'
      expect(path.controller).to eql 'DownloadsController'
      expect(path.action).to eql 'movie'
      expect(path.params).to eql ['id']
    end

    describe 'when is invalid' do
      it 'raise error when unknown paramter(s)' do
        invalid_arguments = { ble: :bla, blo: :bla }
        expect {
          r.draw { match invalid_arguments }
        }.to raise_error(
          ArgumentError,
          "match: problem with arguments #{invalid_arguments}"
        )
      end
    end
  end

  describe '#GET' do
    it 'is valid with valid argument' do
      path = r.draw do
        get '/downloads' => 'downloads#index'
      end.first

      expect(path.method).to eql 'GET'
      expect(path.pattern).to eql '/downloads'
      expect(path.exec).to eql 'downloads#index'
      expect(path.controller).to eql 'DownloadsController'
      expect(path.action).to eql 'index'
      expect(path.params).to eql []
    end

    it 'handles params' do
      r.draw { get '/downloads/:id' => 'downloads#show' }
      expect(r.routing_table.first.params).to eql(['id'])
    end

    it 'handles options' do
      r.draw { get '/downloads' => 'downloads#index', as: :home }
      expect(r.routing_table.first.helper).to eql(:home)
    end
  end

  describe 'params handling' do
    it 'handles to array of params' do
      r.draw do
        get '/hello/:name' => 'home#index'
      end
      expect(r.routing_table.first.params).to eql %w(name)
    end

    it 'handles many params' do
      r.draw do
        get '/book/:name/download/:id' => 'home#index'
      end
      expect(r.routing_table.first.params).to eql %w(name id)
    end
  end

  describe 'execute like Rails' do
    it 'calls controler action' do
      path = r.draw do
        match '/' => 'downloads#index'
      end.first

      class DownloadsController
        def index
          'Hello there'
        end
      end
      expect(path).to be_kind_of(Path)
      expect(path.call_action).to eql('Hello there')
    end

    it 'it works with params' do
      path = r.draw do
        match '/downloads/:id' => 'downloads#index'
      end.first

      class DownloadsController
        def index
          params
        end
      end
      expect(path.call_action).to eql(['id'])
    end
  end

  describe '#draw like Sinatra' do
    describe 'without parameters' do
      before(:each) do
        r.draw do
          get '/' do
            'Hola Mundo!'
          end
        end

        @elem = r.routing_table.first
      end

      it 'handles path' do
        expect(@elem).to be_kind_of(Path)
      end

      it 'handles method' do
        expect(@elem.method).to eql 'GET'
      end

      it 'handles pattern' do
        expect(@elem.pattern).to eql '/'
      end

      it 'can execute block' do
        expect(@elem.call_action).to eql 'Hola Mundo!'
      end
    end

    describe 'with parameters' do
      before(:each) do
        r.draw do
          get '/hello/:name' do
            params
          end
        end
        @elem = r.routing_table.first
      end

      it 'handles path' do
        expect(@elem.call_action).to eql ['name']
      end
    end
  end

  describe '#route' do
    it 'adds route' do
      expect{
        r.send(:route, '/', :hello_world)
      }.to change {
        r.routing_table.size
      }.from(0).to(1)
    end

    it 'creates new path' do
      expect(Path).to receive(:new).with(r, '/', :hello_world).exactly(1).times
      r.send(:route, '/', :hello_world)
    end
  end
end
