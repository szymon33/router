require 'spec_helper'
require 'path'
require 'router'

describe Path do
  let(:path) { Path.new(self, '/', method: 'get') }

  it { respond_to :exec }
  it { respond_to :pattern }
  it { respond_to :helper }
  it { respond_to :method }
  it { respond_to :controller }
  it { respond_to :action }
  it { respond_to :params }
  it { respond_to :block }
  it { respond_to :regexp }

  describe '#initialize' do
    it 'sets @method' do
      expect(path.method).to eql 'get'
    end

    it 'calls sinatra syntax' do
      expect_any_instance_of(described_class).to receive(:sinatra_syntax)
      path
    end

    it 'calls rails_syntax' do
      expect_any_instance_of(described_class).to receive(:rails_syntax)
      Path.new(self, { '/' => 'home#index' }, method: 'get')
    end

    it 'calls set_params' do
      expect_any_instance_of(described_class).to receive(:set_params)
      Path.new(self, { '/movies/:id' => 'home#show' }, method: 'get')
    end
  end

  describe 'when call like route' do
    let(:path) do
      Path.new(self, '/hello', :hello_world)
    end

    it 'sets pattern' do
      expect(path.pattern).to eql '/hello'
    end
  end

  describe '#print' do
    let(:path) { Path.new(self, '/', method: 'get') }
    it 'returns a hash' do
      expect(path.print).to be_a(Hash)
    end

    it 'returns a hash with method' do
      expect(path.print).to include(method: 'get', pattern: '/')
    end
  end

  describe '#sinatra_syntax' do
    before(:each) do
      allow_any_instance_of(described_class).to receive(:validate)
      @path = Path.new self, '/', method: 'post' do
        'hola'
      end
    end

    it 'sets @pattern' do
      expect{
        @path.send(:sinatra_syntax, 'foo', block: 'bar')
      }.to change{
        @path.pattern
      }.from('/').to('foo')
    end

    it 'sets @block' do
      expect{
        @path.send(:sinatra_syntax, 'foo', block: 'bar')
      }.to change{
        @path.block
      }.to('bar')
    end

    it 'calls validate' do
      expect_any_instance_of(described_class).to receive(:validate)
      Path.new self, '/', method: 'post' do
      end
    end
  end

  describe '#validate' do
    it 'create Validate class instance' do
      expect(Validate).to receive(:new).exactly(2).times
      path.send(:validate)
    end
  end

  describe '#set_params' do
    it 'returns nil when no @pattern' do
      path.instance_variable_set(:@pattern, nil)
      expect(path.send(:set_params)).to be_nil
    end

    it 'sets @params' do
      path.instance_variable_set(:@pattern, '/home/:foo/:bar')
      expect(path.send(:set_params)).to eql %w(foo bar)
    end
  end

  describe '#set_controller_action' do
    it 'returns nil when no @pattern' do
      path.instance_variable_set(:@exec, nil)
      expect(path.send(:set_controller_action)).to be_nil
    end

    it 'sets @controller' do
      path.instance_variable_set(:@exec, 'home#index')
      expect{
        path.send(:set_controller_action)
      }.to change{
        path.controller
      }.from(nil).to('HomeController')
    end

    it 'sets @action' do
      path.instance_variable_set(:@exec, 'home#index')
      expect{
        path.send(:set_controller_action)
      }.to change{
        path.action
      }.from(nil).to('index')
    end
  end

  describe 'with symbol argument' do
    it 'can call global defined method' do
      path = Path.new self, '/hello', :hello_global_world
      expect(path.call_action).to eql 'Hola Mundo!'
    end

    it 'can call route - specific method' do
      r = Router.new
      r.draw do
        def hello_from_router
          'hello from router!'
        end

        route '/hello', :hello_from_router
      end
      path = r.routing_table.first
      expect(r.hello_from_router).to eql('hello from router!')
      expect(path.call_action).to eql 'hello from router!'
    end
  end

  describe 'with regular expression' do
    it 'pattern is regular expression' do
      path = Path.new(self, /\w+/, method: 'get')
      expect(path.pattern).to be_a Regexp
    end
  end
end

def hello_global_world
  'Hola Mundo!'
end
