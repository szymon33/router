require 'spec_helper'
require 'validate'

describe Validate do
  it { respond_to :REQUEST_METHODS }

  describe '#validate_exec' do
    it 'with valid argument' do
      expect(
        described_class.new(exec: 'downloads#index')
      ).to be_kind_of(described_class)
    end

    it 'with invlalid argument' do
      expect{
        described_class.new(exec: 'blabla')
      }.to raise_error SyntaxError
    end
  end

  describe '#validate_pattern' do
    it 'in root case' do
      expect(
        described_class.new(pattern: '/')
      ).to be_kind_of(described_class)
    end

    it 'with valid argument' do
      expect(
        described_class.new(pattern: '/admin/subjects/:id/edit')
      ).to be_a(described_class)
    end

    it 'with invlalid argument' do
      expect{
        described_class.new(pattern: '/blabla/')
      }.to raise_error SyntaxError
    end
  end

  describe '#validate_helper' do
    it 'with valid argument' do
      expect(
        described_class.new(helper: :movie)
      ).to be_kind_of(described_class)
    end

    it 'with invlalid argument' do
      expect{
        described_class.new(helper: '<b>')
      }.to raise_error SyntaxError
    end
  end

  describe '#validate_method' do
    let(:validate) { described_class.new({}) }

    it 'with valid argument' do
      validate = described_class.new({})
      expect(
        validate.send(:validate_method, 'GET')
      ).to be_truthy
    end

    it 'with invlalid argument' do
      expect{
        validate.send(:validate_method, 'insert')
      }.to raise_error SyntaxError
    end

    it 'returns nil when nil argument' do
      expect(
        validate.send(:validate_method, nil)
      ).to be_nil
    end

    describe 'when called from initialzer' do
      it 'is called with argument' do
        expect_any_instance_of(described_class).to receive(
          :validate_method).with(:GET)
        described_class.new(method: :GET)
      end
    end
  end
end
