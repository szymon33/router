require 'spec_helper'
require 'application'

describe Application do
  let(:app) { Application.new }

  it { respond_to :routes }

  it 'is valid' do
    expect(app).to be_a(Application)
  end

  it 'routes is a Router' do
    app = Application.new
    expect(app.routes).to be_a(Router)
  end

  it 'takes symbol and evaluate it' do
    path = app.routes.draw do
      def hello_world
        'Hola Mundo!'
      end
      route '/hello/world', :hello_world
    end.first
    expect(path.call_action).to eql 'Hola Mundo!'
  end
end
