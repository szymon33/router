# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name        = 'router'
  s.version     = '1.0.0'
  s.date        = '2015-12-31'
  s.summary     = 'Ruby application router implementation'
  s.description = 'This is a challenge task. Just for fun with Ruby.'
  s.authors     = ['Szymon Miedziejko']
  s.email       = 'miedziejko@gmail.com'
  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- spec/*`.split("\n")
  s.require_paths = ["lib"]
  s.required_ruby_version = '>= 2.1.0'
  s.homepage    = 'https://bitbucket.org/szymon33/router'
  s.license     = 'MIT'
end
