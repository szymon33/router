# My Application Framework (Ruby)

## Introduction

You have been tasked with rebuilding developer community from the ashes of a hypothetical nuclear war, which wiped out all software and it's source code from the face of our planet.

One of the things that has been marked as high priority by The New Earth Committee is designing and implementing a new micro web framework. You have been hand-picked as the one to make this happen!

### Requirements

The framework should be build on top of Rack and fully support nested route traversal with string, regexp­- and parameter ­based (e.g. `/hello/:name`) route definitions.

Route handlers can be provided as blocks (like in Sinatra), references to blocks, action controllers (like in Rails) or plain symbol references (e.g.  `route '/hello', :hello_world`) . The action controllers should support common resource idioms, like `index`,  `create`,  `destroy`,  `update` and  `show`.

It should be possible to define all the routing in a single file, while at the same time allowing developers to spread route handlers across multiple Ruby files. In other words, support large, neatly organised web applications.

Make it easy for other developers to start working on the project. Specifically, provide Ruby gem definition and a README file describing the project and the API of the framework in particular.

Value quality over quantity. Follow [unofficial Ruby style](https://github.com/bbatsov/ruby-style-guide) guide and use [Rubocop](https://github.com/bbatsov/rubocop) to enforce it. Your work should be available via GitHub or BitBucket as a private repository. Commit often.

Finally, when in doubt – especially about the scope of the task - ask!

### Bonus

* Support both Rack and custom middleware. Middleware can be both global and route - specific.

* Support Rack route test helpers.

* Support custom error handling. It should be possible to intercept exceptions and render an error response accordingly.

* Include tests validating the correctness of the framework.

## Implementation

This repository is a start point of implementation gem `router`.

Framework work with Ruby version 2.1. You can add it to your Gemfile with:

```ruby
gem 'router', git: 'git@bitbucket.org:szymon33/router.git', branch: "master", tag: 'v1.0.1'
```

Run the bundle command to install it.

You can review usage of this gem in the **[sample application](https://bitbucket.org/szymon33/router-example)**. Please note that I use git's tags here.

Both repositories are private and not published [on the RubyGems.org site](https://rubygems.org/gems/hola). If there is any problem with `bundler` then you could install `router` gem like the following as well:

```ruby
git clone git@bitbucket.org:szymon33/router-example.git

gem build router.gemspec

gem install router

```

### API

The biggest effort I put into implementing of route patterns. Route definition could be both [Ruby on Rails](http://rubyonrails.org/) or [Sinatra](https://github.com/sinatra/sinatra) style.

**`draw` method from `Router` class**

`draw` method could be called as creator of application routing table

```
    router = Router.new

    router.draw do
      match '/' => 'downloads#index'
    end
```

or as presenter of existing routing tables 

```
    router.draw
```

Above will print routing table similar to Rails `rake routes` using `puts` command.

**Implemented Ruby On Rails routing style**

In general, you should use the get, post, put, patch and delete methods to constrain a route to a particular verb. You can use the match method with the :via option to match multiple verbs at once:

```
    match 'photos', to: 'photos#show', via: 'get'
```

You can also use ```:as``` to define helper

```
    match 'photos', to: 'photos#show', via: 'get', as: 'gallery'
```

You can use GET POST PUT PATCH DELETE like this

```
    get '/patients/:id', to: 'patients#show', as: 'patient'
```

**Implemented Sinatra routing style**

Each route is associated with a block with specific HTML request methods:

```
    get '/' do
      .. show something ..
    end

    post '/' do
      .. create something ..
    end

    put '/' do
      .. replace something ..
    end

    patch '/' do
      .. modify something ..
    end

    delete '/' do
      .. annihilate something ..
    end
```

Route patterns may include named parameters, accessible via the params hash. For now just a list of params:

```
    get '/hello/:name' do
      "Hello #{params}!"
    end
```

`route` method from the task specification:

```
    def hello_world
      'Hola Mundo!'
    end

    route /hello', :hello_world
```
It has to be `hello_world` method in global scope defined.

**Above cases could be observed in specs as well.**

### Automatic Tests

I use [Rspec](http://rspec.info/) for tests. API was developed facing [TDD](https://en.wikipedia.org/wiki/Test-driven_development) approach. Installed gem [simplecov](https://github.com/colszowka/simplecov) nearly 100% coverage.

### Custom error handling

I implemented roughly error handling in terms of routing file. Mostly ArgumentErrors and Syntax Errors. There is Validation class for this propose.

### ToDo

There is still more todo if you want to implement framework like Rails or Sinatra from the ashes by yourself even in scope of their well known basic functionality :smile:

Create git PR or issue so we could take about it.
