require 'router'

# main application framework class
class Application
  attr_accessor :routes

  def initialize
    self.routes = Router.new
  end
end
