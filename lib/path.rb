require 'validate'

# single Path from routint table implementation
class Path
  attr_reader :exec, :pattern, :helper, :method, :controller, :action,
              :params, :block, :symbol, :regexp

  def initialize(parent, arg, options)
    @parent = parent
    @method = options[:method] if options.is_a?(Hash)
    case
    when arg.is_a?(String) || arg.is_a?(Regexp)
      sinatra_syntax(arg, options)
    when arg.is_a?(Hash)
      rails_syntax(arg)
    end
    set_params
  end

  def call_action
    case
    when @controller && @action
      controller_instance.send(action)
    when @block
      instance_eval(&block)
    when @symbol && @parent.respond_to?(symbol)
      @parent.send(@symbol)
    when @symbol && defined?(@symbol)
      Object.send(@symbol)
    end
  end

  def print
    {
      method:     @method,
      pattern:    @pattern,
      exec:       @exec,
      helper:     @helper,
      controller: @controller,
      action:     @action,
      params:     @params
    }
  end

  private

  def sinatra_syntax(arg, options)
    @pattern = arg
    if options.is_a?(Symbol)
      @symbol = options
    else
      @block = options[:block]
    end
    validate
  end

  def rails_syntax(arg)
    @method ||= arg.delete(:via) if arg[:via]
    @helper = arg.delete(:as) if arg[:as]
    if arg.size == 1
      @pattern = arg.first[0]
      @exec = arg.first[1]
    else
      fail ArgumentError, "match: problem with arguments #{arg}"
    end
    validate
    set_controller_action
  end

  # raises error when something want wrong
  def validate
    Validate.new(
      exec:    @exec,
      pattern: @pattern,
      helper:  @helper,
      method:  @method
    )
  end

  def set_params
    return unless @pattern && @pattern.is_a?(String)

    @params = @pattern.scan(/\/\:(\w+)/)
    @params.flatten!
  end

  def set_controller_action
    return unless @exec

    temp = /^([a-z]+)#([a-z]+)$/.match(@exec)
    @controller = "#{temp[1].capitalize}Controller"
    @action = temp[2]
  end

  def controller_instance
    klass = Object.const_get(@controller)
    klass.class_eval { attr_accessor :params }
    c = klass.new
    c.params = params
    c
  end
end
