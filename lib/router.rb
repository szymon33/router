# Application Router class
class Router
  attr_reader :routing_table

  REQUEST_METHODS = %w(GET POST PUT PATCH DELETE)

  require 'path'

  def initialize
    @routing_table = []
  end

  # setup routing table or just print existing routes
  def draw(&block)
    if block_given?
      instance_eval(&block)
    else
      routing_table.each { |line| print line }
    end
  end

  # separate presentation layer
  def print(arg)
    puts arg
  end

  private

  REQUEST_METHODS.each do |m|
    define_method m.downcase do |arg, &block|
      routing_table.push(Path.new(self, arg, method: m, block: block))
    end
  end

  # implementation of Rails match method, in ex.:
  # match "/downloads/:id/movie" => "downloads#movie", as: 'movie', :via: :get
  def match(arg, options = {})
    routing_table.push(Path.new(self, arg, options))
  end

  def route(pattern, symbol)
    routing_table.push(Path.new(self, pattern, symbol))
  end
end
