# Validation of routing path class
class Validate
  REQUEST_METHODS = %w(GET POST PUT PATCH DELETE)

  def initialize(hash)
    hash.each { |k, v| send "validate_#{k}", v }
  end

  private

  # downloads#movie
  def validate_exec(str)
    return unless str
    fail(SyntaxError, str) unless /^[a-z]+#[a-z]+$/.match str
    true
  end

  # /downloads/:id/movie
  def validate_pattern(str)
    return unless str && str.is_a?(String)
    return if /^\/$/.match(str) # root path case
    fail(SyntaxError, str) unless /^(\/:?[a-z]+){1,}$/.match str
    true
  end

  def validate_helper(str)
    return unless str
    fail(SyntaxError, str) unless /^[a-z]+$/.match str
    true
  end

  # method paramter could be both string or symbol
  def validate_method(str)
    return if str.nil?
    str = str.to_s.upcase
    fail SyntaxError unless REQUEST_METHODS.include?(str)
    true
  end
end
