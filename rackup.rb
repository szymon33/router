#!/usr/bin/env ruby

load 'application.rb'
Dir["controllers/*.rb"].each {|file| load file }

@app = Application.new

load 'config/routes.rb'

load ARGV.first
